package main.java.pl.edu.pjwstk.mpr.lab5.domain;

/**
 * Created by Krzysztof Pawlowski on 22/11/15.
 */
public class Address {

    private String streetName;
    private Integer houseNumber;
    private Integer flatNumber;
    private String city;
    private String postCode;
    private String country;

    public Address(String streetName, Integer houseNumber, Integer flatNumber, String city, String postCode, String country) {
        this.streetName = streetName;
        this.houseNumber = houseNumber;
        this.flatNumber = flatNumber;
        this.city = city;
        this.postCode = postCode;
        this.country = country;
    }

    public String getStreetName() {
        return streetName;
    }


    public Integer getHouseNumber() {
        return houseNumber;
    }



    public Integer getFlatNumber() {
        return flatNumber;
    }


    public String getCity() {
        return city;
    }



    public String getPostCode() {
        return postCode;
    }



    public String getCountry() {
        return country;
    }


}
