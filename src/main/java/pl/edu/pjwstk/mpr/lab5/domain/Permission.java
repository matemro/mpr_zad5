package main.java.pl.edu.pjwstk.mpr.lab5.domain;

/**
 * Created by Krzysztof Pawlowski on 22/11/15.
 */
public class Permission {
    private String name;

    public Permission(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }


}
