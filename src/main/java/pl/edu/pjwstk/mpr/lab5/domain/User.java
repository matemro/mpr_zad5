package main.java.pl.edu.pjwstk.mpr.lab5.domain;

/**
 * Created by Krzysztof Pawlowski on 22/11/15.
 */
public class User {

    private String name;
    private String password;
    private Person personDetails;

    public User(String name, String password, Person personDetails){
        this.name = name;
        this.password = password;
        this.personDetails = personDetails;
    }

    public String getName() {
        return name;
    }



    public String getPassword() {
        return password;
    }


    public Person getPersonDetails() {
        return personDetails;
    }


}
