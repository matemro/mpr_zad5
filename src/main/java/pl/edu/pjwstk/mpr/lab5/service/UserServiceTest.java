package main.java.pl.edu.pjwstk.mpr.lab5.service;

import main.java.pl.edu.pjwstk.mpr.lab5.domain.*;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Created by Uzytkownik on 2018-01-25.
 */
class UserServiceTest {


    private static List<Person> persons;
    private static List<User> users;
    private User user1, user2, user3;
    private Person person1, person2, person3;
    private Role role1;
    private Address address1, address2, address3;
    private Permission permission1, permission2, permission3;


    public UserServiceTest() {

        address1 = new Address("Kołątaja", 3, 5, "Warszawa", "44-121", "Polska");
        address2 = new Address("Padewskiego", 5, 7, "Gdańsk", "92-861", "Węgry");
        address3 = new Address("Mickiewicza", 1, 7, "Sopot", "55-781", "Polska");
        permission1 = new Permission("Dostep do szafek");
        permission2 = new Permission("Dostęp do drzwi");
        permission3 = new Permission("Dostęp do okien");
        String s1 = new String("123456");
        String s2 = new String("612351");
        String s3 = new String("612431");
        role1 = new Role("Sprzatacz", Arrays.<Permission>asList(permission1, permission2, permission3));
        person1 = new Person("Antek", "Brzeszczak", Arrays.<String>asList(s1, s3), Arrays.<Address>asList(address1, address3), role1, 35);
        person2 = new Person("Bogdan", "Kowal", Arrays.<String>asList(s3, s1), Arrays.<Address>asList(address2, address3), role1, 32);
        person3 = new Person("Julian", "Nowak", Arrays.<String>asList(s2, s3, s1), Arrays.<Address>asList(address1, address2, address3), role1, 10);
        persons = Arrays.asList(person1, person2, person3);
        user1 = new User("Jan", "123", person1);
        user2 = new User("Bogdan", "221", person2);
        user3 = new User("Julian1", "312", person3);
        users = Arrays.asList(user1, user2, user3);

    }


    @org.junit.jupiter.api.Test
    void findUsersWhoHaveMoreThanOneAddress() {
        assertEquals(Arrays.asList(user1, user2, user3), UserService.findUsersWhoHaveMoreThanOneAddress(users));
    }

    @org.junit.jupiter.api.Test
    void findOldestPerson() {
        assertEquals(person1, UserService.findOldestPerson(users));
    }

    @org.junit.jupiter.api.Test
    void findUserWithLongestUsername() {
        assertEquals(user3, UserService.findUserWithLongestUsername(users));
    }

    @org.junit.jupiter.api.Test
    void getNamesAndSurnamesCommaSeparatedOfAllUsersAbove18() {
        UserService.getNamesAndSurnamesCommaSeparatedOfAllUsersAbove18(users);
    }

    @org.junit.jupiter.api.Test
    void getSortedPermissionsOfUsersWithNameStartingWithA() {
        assertEquals(Arrays.asList( permission1.getName(), permission2.getName(),permission3.getName()), UserService.getSortedPermissionsOfUsersWithNameStartingWithA(users));
    }

    @org.junit.jupiter.api.Test
    void printCapitalizedPermissionNamesOfUsersWithSurnameStartingWithS() {
        UserService.printCapitalizedPermissionNamesOfUsersWithSurnameStartingWithS(users);
    }

    @org.junit.jupiter.api.Test
    void groupUsersByRole() {
        UserService.groupUsersByRole(users);
    }

    @org.junit.jupiter.api.Test
    void partitionUserByUnderAndOver18() {
        UserService.partitionUserByUnderAndOver18(users);
    }


}