package main.java.pl.edu.pjwstk.mpr.lab5.domain;

import java.util.List;

/**
 * Created by Krzysztof Pawlowski on 22/11/15.
 */
public class Role {

    private String name;



    List<Permission> permissions;
    public Role(String name, List<Permission> permissions) {
        this.name = name;
        this.permissions = permissions;
    }
    public String getName() {
        return name;
    }



    public List<Permission> getPermissions() {
        return permissions;
    }


}
